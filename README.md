Rules Condition Environment
================
This module provides a Rules condition plugin. This enables you to check
whether or not the base url matches a configured url.

Background
------------
When posting data to third party api's, it may be desirable to restrict how a
rule behaves depending on your environment. Most environments have a different
uri or base url. Exposing this as a condition allows rules to be restricted on
this basis. 

One use case is that after syncing a database from a production environment, 
we likely want to prevent developers inadvertently posting to certain api
endpoints. This will likely be conditional on their environment.

Installation
------------
Download the module like any other Drupal module.

Author
-----------
* Daniel Lobo (2dareis2do)
