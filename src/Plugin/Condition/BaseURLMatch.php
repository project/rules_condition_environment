<?php

namespace Drupal\rules_condition_environment\Plugin\Condition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides an 'Base URL Match' condition.
 *
 * @Condition(
 *   id = "rules_base_url_match",
 *   label = @Translation("Base URL Match"),
 *   category = @Translation("Environment"),
 *   context_definitions = {
 *     "url" = @ContextDefinition("string",
 *       label = @Translation("Base URL Match"),
 *       description = @Translation("Determine if configured url matches Base
 *  URL. If it does not match, skip the condition."),
 *       default_value = NULL,
 *       required = FALSE
 *     )
 *   }
 * )
 */
class BaseURLMatch extends RulesConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The corresponding request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * Constructs a BaseURLMatch object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The corresponding request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $request_stack;
  }

  /**
   * Checks if an base URL matches configured url.
   *
   * @param string $url
   *   The configured base url address to check.
   *
   * @return bool
   *   TRUE if the configured URL matches the base url or host.
   */
  protected function doEvaluate($url = NULL) {
    $base_url = $this->requestStack->getCurrentRequest()->getHost();
    if (!isset($url)) {
      $url = $this->requestStack->getCurrentRequest()->getHost();
    }
    if ($url === $base_url) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

}
